<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
});
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML'></script>

#Dynamic Erdős-Pósa listing


This page is an attempt to construct an exhaustive listing of results about the [Erdős--Pósa property](https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93P%C3%B3sa_theorem#Erd%C5%91s%E2%80%93P%C3%B3sa_property) for graphs. Some data has been collected when preparing [this article](http://dx.doi.org/10.1016/j.dam.2016.12.025).  
Note that **the papers listed on this page have not all been peer-reviewed** (yet). All of them are availlable online or have been published in a journal or conference proceedings with peer-review.  
If you notice a missing result, a typo, a broken link, or an outdated reference, please [contact me](../index.html) and I will update the list.

[MathJax](https://www.mathjax.org) is used for rendering formulas, so you need to have JavaScript enabled in order to see them.

### Quick links

 * positive results: [acyclic patterns](#acyclic-patterns), [triangles](#triangles), [**cycles**](#cycles), [cycles with length constraints](#cycles-with-length-constraints), [cycles with prescribed vertices](#cycles-with-prescribed-vertices), [**minors**](#minors), [topological minors](#topological-minors), [immersions](#immersions), [induced patterns](#induced-patterns), [patterns with prescribed positions](#patterns-with-prescribed-positions), [classes with bounded parameters](#classes-with-bounded-parameters), [fractional packings](#fractional-packings);
 * negative results: [paths](#lower-bounds-for-paths), [cycles](#lower-bounds-for-cycles), [minors](#lower-bounds-for-minors), [topological minors](#lower-bounds-for-topological-minors), [immersions](#lower-bounds-for-immersions), [induced patterns](#lower-bounds-for-induced-patterns).

	
### Basic definitions

Let $\mathcal{H}$ be a class of graphs and let $G$ be a graph.

  * An _$\mathcal{H}$-vertex-packing_ in $G$ is a collection of vertex-disjoint subgraphs of $G$, each isomorphic to a member of $\mathcal{H}$.
  * An _$\mathcal{H}$-vertex-cover_ in $G$ is a set $X$ of vertices of $G$ such that $G\setminus X$ has no subgraph isomorphic to a member of $\mathcal{H}$.

Let $\mathcal{G}$ be a class of graphs. We say that $\mathcal{H}$ (refered to as the _guest class_) has the _vertex-Erdős--Pósa property_ in the class $\mathcal{G}$ (the _host class_) with _gap_ $f$ if, for every $G \in \mathcal{G}$ and $k \in \mathbb{N}$,

  * either $G$ has an $\mathcal{H}$-vertex-packing of size $k+1$;
  * or $G$ has a $\mathcal{H}$-vertex-covering of size $f(k)$.

The notions of _$\mathcal{H}$-edge-packing_, _$\mathcal{H}$-edge-cover_, and _edge-Erdős--Pósa property_ can be defined similarly by replacing "vertex" by "edge" in the definitions above.

**Notice that a different definition of the gap (with $k+1$ replaced by $k$ above) is sometimes used in the litterature (but not on this page), leading to slightly different values of the gap.**

The fourth column (T.) of the tables below refers to the type of Erdős--Pósa property: _v_ for vertex and _e_ for edge. The definitions of the other variants of the Erdős--Pósa property mentioned below (_w_ for weighted, _v<sub>1/2</sub>_ for vertex half-integral, etc.) are not given here but can be found in the corresponding papers.

Notation of the type $O_t(k)$ and $o_t(k)$ is used to stress that the hidden constants depend on $t$.

## Positive results

### Acyclic patterns

|Ref.    |Guest class    |Host class    |T.    |Gap at most| Remarks
|---     |            ---|--------------|------|---        |---
|[\[Kőn31\]](#Kőn31) | $K_2$ | bipartite | v | $k$
|[\[LY78\]](#LY78) | directed cuts | any digraph | e | $k$
|[\[Lov76\]](#Lov76) | directed cuts | any digraph | e | $k$
|[\[Men27\]](#Men27) | $(S,T)$-paths | any | v/e | $k$
|[\[MNL84\]](#MNL84) | $(S,T)$-paths of length $\geq t$ | any | v | $(3(t + 2) - 5)k$
|[\[HU19\]](#HU19)   | $(S,T)$-paths of length $\geq t$ | any | e | unspecified
|[\[Grü38\]](#Grü38) | directed $(S,T)$-paths | any digraph | v/e | $k$ 
|[\[Gal64\]](#Gal64) | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> | any | v | $2k$
|[\[GV95\]](#GV95) | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span>, $S$ independent | graphs where every block has $\leq 2$ cutvertices and is either a cycle or a clique | v | $k$
|[\[Sam92\]](#Sam92) | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span>, $S$ independent | unicyclic graphs whose cycle has $\leq 2$ vertices of degree $\geq 3$ | v | $k$
|[\[Sam83\]](#Sam83) [\[TV89\]](#TV89) | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span>, $S$ independent | trees | v | $k$
|[\[KKKX20\]](#KKKX20) | <span class="tooltip">directed odd $S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _directed odd $S$-path_ is a directed path of odd length with both endpoints in $S$</span></span> | any | v | $k$
|[\[Mad78b\]](#Mad78b) | <span class="tooltip">$\mathcal{S}$-paths<span class="tooltiptext">$\mathcal{S}$ is a collection of disjoint subsets of $V(G)$ and a _$\mathcal{S}$-path_ is a path connecting different sets in $\mathcal{S}$</span></span> | any | v | unspecified | see [\[Sch01\]](#Sch01)
|[\[Mad78a\]](#Mad78a) | <span class="tooltip">$\mathcal{S}$-paths<span class="tooltiptext">$\mathcal{S}$ is a collection of disjoint subsets of $V(G)$ and a _$\mathcal{S}$-path_ is a path connecting different sets in $\mathcal{S}$</span></span> | any | e | $2k$ | see [\[SS04\]](#SS04), [\[BHJ18a\]](#BHJ18a)
|[\[HU19\]](#HU19)   | <span class="tooltip">$\mathcal{S}$-paths<span class="tooltiptext">$\mathcal{S}$ is a collection of disjoint subsets of $V(G)$ and a _$\mathcal{S}$-path_ is a path connecting different sets in $\mathcal{S}$</span></span> of length $\geq t$ | any | e | unspecified
|[\[GGR+09\]](#GGR+09) | odd <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span>| any | v | $2k$
|[\[BHJ18a\]](#BHJ18a) | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length at least $t$ | any | v | $4kt$
|[\[HU19\]](#HU19)   | <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $\geq t$ | any | e | unspecified
|[\[BHJ18a\]](#BHJ18a) | even <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> | any | v | $10k$
|[\[BU18\]](#BU18) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $0 \mod 4$ | any | v | unspecified
|[\[BU18\]](#BU18) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $2 \mod 4$ | any | v | unspecified
[\[Ulm20\]](#Ulm20) | $S$-paths of length $0 \mod m$, where $m$ is an odd prime | any | v | unspecified
[\[Ulm20\]](#Ulm20) | $S$-paths of length $d \mod m$ if $\forall x,y \in \\{0, \dots, m-1\\}, y \neq 0 \Rightarrow \exists n \in \mathbb{Z}, 2x+ny = d \mod m$ | any | v | unspecified
|[\[CGG+06\]](#CGG+06) | <span class="tooltip">non-zero<span class="tooltiptext">An undirected path in a digraph is _non-zero_ if the sum of the labels of its arcs (with signs depending on the orientation of the edge) is non-zero.</span></span> $S$-paths | edge-group-labeled digraphs | v | $2k$
|[\[Wol10\]](#Wol10) | <span class="tooltip">non-zero<span class="tooltiptext">A path is _non-zero_ if the sum of the labels of its edges is non-zero.</span></span> $S$-paths | edge-group-labeled graphs | v | $50 k^4$
[\[Ulm20\]](#Ulm20) | zero $S$-paths | graphs whose edges are labeled by a finite abelian group | v | unspecified
[\[Ulm20\]](#Ulm20) | $S$-paths of length $\geq t$ and value $\gamma$, where $\gamma \in \Gamma$ | graphs whose edges are labeled by an abelian group $\Gamma$ such that $\forall x,y \in \Gamma, y \neq 0 \Rightarrow \exists n \in \mathbb{Z}, 2x + ny = \gamma$ | v | unspecified
[\[IS17\]](#IS17) | <span class="tooltip">odd $(u,v)$-trails<span class="tooltiptext">An _$(u,v)$-trail_ is a walk from $u$ to $v$ that may have repeated vertices but no repeated edge. A trail is odd if it has an odd number of edges.</span></span> | any | e | $2k+1$
|[\[MW15\]](#MW15) | $H$-valid paths, $H$ with no matching of size $t$ | any | v | $2^{2^{O(k+t)}}$
|[\[FJW13\]](#FJW13) | $\mathcal{M}(H)$, $H$ forest | any | v | $O_{H}(k)$
|[\[BHJ18a\]](#BHJ18a) | <span class="tooltip">$S$-$t$-combs<span class="tooltiptext">An _elementary $t$-comb_ can be obtained by adding a pendant vertex to each internal vertex of a path on $t$ edges. A _$t$-comb_ is any subdivision of an elementary $t$-comb. Let $S\subseteq V(G)$. A _$S$-$t$-comb_ is a $t$-comb whose leaves, and only the leaves, belong to $S$.</span></span> | any | v | $4kt$
[\[SU20\]](#SU20) | <span class="tooltip">$S$-$t$-trees<span class="tooltiptext">$A$ is a vertex set, $t\in \mathbb{N}$ and a _$S$-$t$-tree_ is a tree that contain $m$ vertices of $A$.</span></span> | any | e | $2t^2k^2$



### Triangles

The following table contains results related to [Tuza's conjecture](http://www.openproblemgarden.org/op/triangle_packing_vs_triangle_edge_transversal), that can be seen as Erdős-Pósa type problems.


|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |            ---|--------------|------|---
[\[Tuz90\]](#Tuz90) | triangles | planar graphs |e| $2k$
[\[Tuz90\]](#Tuz90) | triangles | graphs with $m \geq 7n^2/16$ | e| $2k$
[\[Tuz90\]](#Tuz90) | triangles | tripartite graphs | e| $7k/3$ 
[\[Kri95\]](#Kri95) | triangles | <span class="tooltip">$\mathcal{T}(K_{3,3})$<span class="tooltiptext">$\mathcal{T}(H)$ is the class of graphs containing a subdivision of $H$</span></span>-free graphs | e | $2k$
[\[HK88\]](#HK88)   | triangles | tripartite graphs | e | $1.956k$
[\[Hax99\]](#Hax99) | triangles | any | e | $(3-\frac{3}{23})k$
[\[ALBT11\]](#ALBT11) | triangles | odd-wheel-free graphs | e | $2k$
[\[ALBT11\]](#ALBT11) | triangles | 4-colorable graphs | e | $2k$ 
[\[HKT11\]](#HKT11) | triangles | $K_4$-free planar graphs| e | $3k/2$
[\[HKT11\]](#HKT11) | triangles | $K_4$-free <span class="tooltip">flat<span class="tooltiptext">A graph is _flat_ if every edge belongs to at most two triangles.</span></span> graphs| e | $3k/2$
[\[Pul15\]](#Pul15) | triangles | graphs with $\mathrm{mad} < 7$ | e | $2k$
[\[LBT16\]](#LBT16) | triangles | graphs whose <span class="tooltip">triangle graph<span class="tooltiptext">The _triangle graph_ of $G$ is the graph whose vertices are triangles of $G$ and where two edges are adjacent if the corresponding triangles share an edge.</span></span> is perfect | e | $2k$ 
[\[BFG21\]](#BFG21) | triangles | graphs $G$ with treewidth $\leq 6$ | e | $2k$
[\[BFG21\]](#BFG21) | triangles | planar triangulations except $K_4$ | e | $1.5k$
[\[BFG21\]](#BFG21) | triangles | maximal graphs of treewidth 3  | e | $\frac{9}{5}k + \frac{1}{5}$
[\[BBG+22\]](#BBG+22) | triangles | threshold graphs | e | $2k$
[\[BBG+22\]](#BBG+22) | triangles | even-balanced co-chain graphs | e | $2k$
[\[Tuz94\]](#Tuz94) | directed triangles | planar oriented graphs | e | $k$
[\[MPT18\]](#MPT18) | directed triangles | directed graphs | e | $2k-1$


### Cycles

For cycles with length or modularity constraints or using prescribed vertices ($S$-cycles), see the next tables.

|Ref.    |Guest class    |Host class    |T.    |Gap at most | Remarks
|---     |            ---|--------------|------|---         |---
[\[Lov65\]](#Lov65) | cycles | graphs without 2 vertex-disjoint cycles | v | =3
[\[Vos69\]](#Vos69) | cycles | graphs without 3 vertex-disjoint cycles | v | =6
[\[Vos67\]](#Vos67) | cycles | graphs without 4 vertex-disjoint cycles | v | 12 | see [\[Vos69\]](#Vos69)
[\[EP65\]](#EP65)   | cycles | any | v | $O(k\log k)$
[\[Sim67\]](#Sim67) | cycles | any | v | $\left (4 + o(1)\right )k \log k$
[\[Vos69\]](#Vos69) | cycles | any | v | $\left (2 + o(1) \right )k \log k$
[\[Vos69\]](#Vos69) | cycles | any | v | $4k \left (1+ \log \left (k + \frac{3}{2} \right ) \right)$
see [\[Die05\]](#Die05) | cycles | any | e | $(2 + o(1))k \log k$
 [\[DZ02\]](#DZ02)  | cycles | weighted graphs with no induced subdivision of $K_{2,3}$, a wheel, or an <span class="tooltip">odd ring<span class="tooltiptext">An _odd ring_ is a graph obtained from an odd cycle by replacing every edge $\{u,v\}$ by either a triangle containing $\{u,v\}$, or by two triangles on vertices $\{u,a,b\}$ and $\{v,c,d\}$ together with the edges $\{b,c\}$ and $\{a,d\}.$</span></span>| w | $k$
[\[DXZ03\]](#DXZ03) | cycles | graphs with no induced subdivision of $K_{3,3}$, a wheel, or an <span class="tooltip">odd ring<span class="tooltiptext">An _odd ring_ is a graph obtained from an odd cycle by replacing every edge $\{u,v\}$ by either a triangle containing $\{u,v\}$, or by two triangles on vertices $\{u,a,b\}$ and $\{v,c,d\}$ together with the edges $\{b,c\}$ and $\{a,d\}.$</span></span> | v | $k$
[\[BD92\]](#BD92) | cycles | planar graphs | v | $54k$
[\[KLL02\]](#KLL02) | cycles | planar graphs | v | $5k$
[\[CHC12\]](#CHC12) [\[MYZ13\]](#MYZ13)[\[CGH14\]](#CGH14) | cycles | planar graphs | v | $3k$
[\[CGH14\]](#CGH14) | cycles | graphs that embed in a closed surface of Euler characteristic $\geq 0$ | v | $3k$
[\[CGH14\]](#CGH14) | cycles | graphs that embed in a closed surface of Euler characteristic $c \leq 0$ | v | $3k - 103c$
[\[KLL02\]](#KLL02) | cycles | outerplanar graphs | v | $2k$
[\[Mun16\]](#Mun16) | cycles | $(K_4, \text{claw}, \text{diamond})$-free graphs | v | $2k$
[\[Mun16\]](#Mun16) | cycles | planar claw-free graphs with $\Delta\leq 4$ | v | $2k$
[\[BDM+19\]](#BDM+19) | cycles | planar subcubic graphs | v | $2k$
[\[MYZ13\]](#MYZ13) | cycles | planar graphs | e | $4k-1$
[\[BD92\]](#BD92) | faces | plane graphs | v | $27k$
[\[BD92\]](#BD92) + [\[MYZ13\]](#MYZ13) | faces | plane graphs | v | $6k$
[\[BD92\]](#BD92) + [\[MYZ13\]](#MYZ13) + [\[GR09\]](#GR09) | <span class="tooltip">cycles or faces<span class="tooltiptext">When one is looking for connected covers in planar graphs, covering faces is equivalent to covering cycles (see Lemma 1 in [\[GR09\]](#GR09)).</span></span> | connected planar graphs with $\delta \geq 3$ | v | $66k - 14$ | the cover additionally induces a connected subgraph
[\[RRST96\]](#RRST96) | directed cycles | any digraph| v | unspecified
[\[MMP+19\]](#MMP+19) | directed cycles | any digraph| v<sub>1/4</sub> | $O(k^4)$
[\[MMP+19\]](#MMP+19) | directed cycles | any digraph| v<sub>1/2</sub> | $O(k^6)$
[\[RS96\]](#RS96)   | directed cycles | planar digraphs| v | $O(k\log(k)\log\log k)$
[\[RS96\]](#RS96) + [\[GW96\]](#GW96)  | directed cycles | planar digraphs| v | $63k$
[\[STV22\]](#STV22) | directed cycles | planar digraphs | v | $12k$
[\[GT11\]](#GT11) | directed cycles | <span class="tooltip">strongly planar digraphs<span class="tooltiptext">A digraph is _strongly planar_ if it has a planar drawing such that for every vertex $v$, the edges with head $v$ form an interval in the cyclic ordering of edges incident with $v$ (definition from [\[GT11\]](#GT11)).</span></span>| v | $k$
[\[GT11\]](#GT11) | directed cycles | digraphs with no <span class="tooltip">butterfly minor<span class="tooltiptext">See [\[GT11\]](#GT11) for a definition.</span></span> isomorphic to an <span class="tooltip">odd double circuit<span class="tooltiptext">An _odd double circuit_ is a digraph obtained from an undirected circuit of odd length more than 2 by replacing each edge by a pair of directed edges, one in each direction.</span></span> or <span class="tooltip">$F_7$<span class="tooltiptext">$F_7$ is the digraph obtained from the directed cycle on vertices $v_1, \dots, v_7,v_1$, by adding the edges creating the directed cycle $v_1,v_3, v_5, v_7, v_2, v_4, v_6, v_1$.</span></span> | v | $k$
[\[LY78\]](#LY78) | directed cycles | planar digraphs | e | $k$
[\[Sey96\]](#Sey96) | directed cycles | eulerian digraphs with a linkless embedding in 3-space | e | $k$
[\[HJW18\]](#HJW18) | cycles non homologous to zero | embedded graphs | v<sub>1/2</sub> | unspecified
[\[STV22\]](#STV22) | any collection of uncrossable cycles | planar graphs or planar digraphs | v | $12k$
[\[STV22\]](#STV22) | any collection of uncrossable cycles | planar graphs or planar digraphs | e | $9.6k$

### Cycles with length constraints

Includes cycles of a certain value with respect to a labeling of the edges or vertices of the graph.

|Ref.    |Guest class    |Host class    |T.    |Gap at most | Remarks
|---     |            ---|--------------|------|---         |---
[\[Ree99\]](#Ree99) | odd cycles | planar graphs | v | superexponential
[\[FHRV06\]](#FHRV06) | odd cycles | planar graphs | v | $10k$
[\[KSS12\]](#KSS12) | odd cycles | planar graphs | v | $\max(0, 6k-2)$
[\[Tho01\]](#Tho01) | odd cycles | $2^{3^{9k}}$-connected graphs | v | $2k$
[\[RR01\]](#RR01) | odd cycles | $576k$-connected graphs | v | $2k$
[\[KR09\]](#KR09) | odd cycles | $24k$-connected graphs | v | $2k$
[\[Ree99\]](#Ree99) | odd cycles | <span class="tooltip">$k$-near bipartite<span class="tooltiptext">A graph is _$k$-near bipartite_ if every set of $p$ vertices contains a stable set of size at least $p/2 - k$.</span></span> graphs | v | unspecified
[\[KN07\]](#KN07) | odd cycles | embeddable in an orientable surface of Euler genus $t$ | v/e | unspecified
[\[BR00\]](#BR00) | odd cycles | planar | e | unspecified
[\[KV04\]](#KV04) [\[FHRV06\]](#FHRV06) | odd cycles | planar graphs | e | $2k$
[\[KK16\]](#KK16) | odd cycles | 4-edge-connected graphs | e | $2^{2^{O(k \log k)}}$
[\[Ree99\]](#Ree99) | odd cycles | any | v<sub>1/2</sub> | unspecified
|[\[BHJ18a\]](#BHJ18a) | even cycles | any | e | <span class="tooltip">$O(k^2 \log k)$<span class="tooltiptext">The function obtained in [\[BHJ18a\]](#BHJ18a) is $k \mapsto 6kh(k)$, where $h$ is the function for the vertex Erdős-Pósa property of even cycles. It was later proved that $h(k) = O(k  \log k)$ (see below, stated for cycles of length $0 \mod t$).</span></span>
[\[CJU19\]](#CJU19) | even cycles | any | e | $O(k \log k)$
[\[Tho88\]](#Tho88) | cycles of length $0 \mod t$ | any | v | $2^{t^{O(k)}}$
[\[CC13\]](#CC13) | cycles of length $0 \mod t$ | any | v | $k~ \mathrm{polylog}~k \cdot 2^{\mathrm{poly}(t)}$
[\[CHJR18\]](#CHJR18) | cycles of length $0 \mod t$ | any | v | $O_t(k \log k)$
[\[CHJR18\]](#CHJR18) | cycles of length $0 \mod t$ | any proper minor-closed class $\mathcal{F}$ | v | $O_{t,\mathcal{F}}(k)$
[\[KW06\]](#KW06) | non-zero cycles | $(15k/2)$-connected group-labeled graphs | v | $2k$
[\[Wol11\]](#Wol11) | non-zero cycles | group-labeled graphs, c.f. [\[Wol11\]](#Wol11) | v | $c^{k^{c'}}$ for some $c,c'$
[\[Wol11\]](#Wol11) | cycles of non-zero length mod $2t+1$ | any | v | $c^{k^{c'}}$ for some $c,c'$
[\[HJW18\]](#HJW18) | doubly non-zero cycles, c.f. [\[HJW18\]](#HJW18) | doubly group-labeled graphs | v<sub>1/2</sub> | unspecified
[\[HJW18\]](#HJW18) | odd cycles non homologous to zero | embedded graphs | v<sub>1/2</sub> | unspecified
[\[GHK+21\]](#GHK+21) | cycles belonging to certain $\mathbb{Z}_2$-homotopy classes of $\Sigma$ | graphs embedded on a surface $\Sigma$ | v<sub>1/2</sub> | unspecified
[\[GHK+21\]](#GHK+21) | cycles whose <span class="tooltip">value<span class="tooltiptext">The value of a cycle (with respect to an edge-labeling with the elements of some group) is the sum of the labels of its edges.</span></span> equals a certain value | graphs whose edges are labeled by a finite abelian group | v<sub>1/2</sub> | unspecified
[\[GHK+21\]](#GHK+21) | cycles whose <span class="tooltip">values<span class="tooltiptext">The value of a cycle (with respect to an edge-labeling with the elements of some group) is the sum of the labels of its edges.</span></span> avoid a <span class="tooltip">finite number of values<span class="tooltiptext">For each of the groups used in the labeling, a finite number of values to avoid is fixed at the beginning.</span></span> | graphs whose edges are labeled with finitely many abelian groups | v<sub>1/2</sub> | unspecified | besides $k$, the gap only depends on the _number_ of groups and of values to avoid
[\[GHK+21\]](#GHK+21) | cycles whose <span class="tooltip">values<span class="tooltiptext">The value of a cycle (with respect to a vertex-labeling with the elements of some group) is the sum of the labels of its vertices.</span></span> avoid a <span class="tooltip">finite number of values<span class="tooltiptext">For each of the groups used in the labeling, a finite number of values to avoid is fixed at the beginning.</span></span> | graphs whose vertices are labeled with finitely many abelian groups | v<sub>1/2</sub> | unspecified | besides $k$, the gap only depends on the _number_ of groups and of values to avoid
[\[Bir03\]](#Bir03) | cycles of length $\geq 4$ | graphs without 2 vertex-disjoint cycles of length $\geq 4$ | v | =4
[\[Bir03\]](#Bir03) | cycles of length $\geq 5$ | graphs without 2 vertex-disjoint cycles of length $\geq 5$ | v | =5
[\[BBR07\]](#BBR07) | cycles of length $\geq t$ | any | v | $(13+o_t(1))tk^2$ 
[\[FH14\]](#FH14)   | cycles of length $\geq t$ | any | v | $(6t+4+o_t(1))k\log k$
[\[MNŠW17\]](#MNŠW17) |cycles of length $\geq t$ | any | v | $6kt + (10 + o(1)) k \log k$
[\[BHJ16\]](#BHJ16) | cycles of length $\geq t$ | any | e | $O(k^2 \log k + kt)$
[\[CJU19\]](#CJU19) | cycles of length $\geq t$ | any | e | $8k(t−1)(\log_2(kt) + 1)$
[\[KKKX20\]](#KKKX20) | directed odd cycles | any digraph | v<sub>1/2</sub> | unspecified
[\[HM13\]](#HM13) | directed cycles of length $\geq 3$ | any digraph | v | unspecified
[\[AKKW16\]](#AKKW16) | directed cycles of length $\geq t$ | any digraph| v | unspecified

### Cycles with prescribed vertices or edges

For a set $S \subseteq V(G)$, an _$S$-cycle_ of $G$ is a cycle of $G$ containing at least one vertex of $S$. The results listed in this section relate, for any $S \subseteq V(G)$, the maximum number of $S$-cycles with the minimum number of vertices whose removal destroys all $S$-cycles. This setting extends to cycles that of $S$-paths mentioned in the acyclic patterns section.


|Ref.    |Guest class    |Host class    |T.    |Gap at most | Remarks
|---     |            ---|--------------|------|---         |---
[\[KKM11\]](#KKM11) | $S$-cycles | any | v | $40k^2 \log k$
[\[PW12\]](#PW12) | $S$-cycles | any | v/e | $O(k \log k)$
[\[KKL19\]](#KKL19) | $S$-cycles | graphs without two vertex-disjoint $S$-cycles | v | =4
[\[BJS17\]](#BJS17) | $S$-cycles of length at least $t$ | any | v | $O(tk\log k)$ 
[\[Joo14\]](#Joo14) | odd $S$-cycles | $50k$-connected graphs | v | $O(k)$ 
[\[KK13\]](#KK13) | odd $S$-cycles | any | v<sub>1/2</sub> | unspecified
[\[Bru19\]](#Bru19) | even $S$-cycles | any | e | unspecified
[\[KK12\]](#KK12) | directed $S$-cycles | all digraphs | v<sub>1/5</sub> | unspecified
[\[KKKK13\]](#KKKK13) | odd directed $S$-cycles | any digraph | v<sub>1/2</sub> | unspecified
[\[HJW18\]](#HJW18) | <span class="tooltip">$(S,S')$-cycles<span class="tooltiptext">$S,S' \subseteq V(G)$ and an _$(S,S')$-cycle_ is a cycle that contains vertices of both $S$ and $S'$</span></span> | any | v | unspecified
[\[GHK+21\]](#GHK+21) | cycles that intersect at least $t_i\leq t$ times the set $S_i$ for every $i \in \\{1, \dots, m\\}$, for a collection $S_1, \dots, S_m$ of subsets of vertices of the graph | any | v<sub>1/2</sub> | unspecified | besides $k$, the gap only depends on $m$ and $t$
[\[STV22\]](#STV22) | <span class="tooltip">$(=1)$-edge-$S$-cycles<span class="tooltiptext">Here $S$ is a set of edges and we consider those cycles that contain exactly one edge from $S$.</span></span> | planar graphs | v| 12

### Minors

For every graph $H$, we denote by $\mathcal{M}(H)$ the class of graphs that can be contracted to~$H$. (So $H$ is a minor of $G$ iff some subgraph of $G$ is isomorphic to a graph in $\mathcal{M}(H)$.)
For every digraph $D$, we denote by $\vec{\mathcal{M}}_b(D)$
(respectively $\vec{\mathcal{M}}_s(D)$) the class of all digraphs that
contain $D$ as a butterfly contraction (respectively strong contraction).

For every $t \in \mathbb{N}$, $\theta_t$ is the multigraph with two vertices connected by one edge of multiplicity $t$.
For every $t\in \mathbb{N}$, a digraph is said to be _$t$-semicomplete_ if for every vertex $v$ there are at most $t$ vertices that are not connected to $v$ by an arc (in either direction). A _semicomplete_ digraph is a 0-semicomplete digraph.


|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |---         ---|--------------|------|---
[\[Sim67\]](#Sim67) | $\mathcal{M}($complement of ($K_{3,3}$ minus an edge)) | any | v | $(4000+o(1))k \log k$
[\[RS86\]](#RS86) | $\mathcal{M}(H)$, $H$ planar | any | v |unspecified
[\[RS86\]](#RS86) | $\mathcal{M}(H)$, $H$ planar with $q$ connected components| $\{G, \mathbf{tw}(G)\leq t\}$ | v | $(t-1)(kq-1)$
[\[FST11\]](#FST11) | $\mathcal{M}(H)$, $H$ planar connected | any proper minor-closed class $\mathcal{F}$ | v | $O_{H,\mathcal{F}}(k)$
[\[DKW12\]](#DKW12) | $\mathcal{M}(K_t)$ | $O(kt)$-connected graphs | v | unspecified
[\[FLM+13\]](#FLM+13) | $\mathcal{M}(\theta_t)$ | any | v | $O(t^2k^2)$
[\[RT13\]](#RT13) | $\mathcal{M}(H), \mathbf{pw}(H) \leq 2$ and $H$ connected on $h$ vertices | any | v | $2^{O(h^2)}\cdot k^2\log k$
[\[CC13\]](#CC13) + [\[CC14\]](#CC14) | $\mathcal{M}(H)$, $H$ planar connected on $h$ vertices | any | v | $\mathrm{poly}(h)\cdot k\cdot \mathrm{polylog}~k$
[\[RST16\]](#RST16) | $\mathcal{M}(\theta_t)$ | any | e | $k^2t^2 \mathrm{polylog}~kt$
[\[RST16\]](#RST16) | $\mathcal{M}(\theta_t)$ | any | e | $k^4t^2 \mathrm{polylog}~kt$
[\[SU20\]](#SU20) | $\mathcal{M}(H)$ where $H$ is the $2 \times 3$ grid | any | e| $O(k^3 \log k)$
[\[SU20\]](#SU20) | $\mathcal{M}(\text{house graph})$ | any | e| $O(k^2 \log k)$
[\[AKKW16\]](#AKKW16) | $\vec{\mathcal{M}}_{b}(H)$, $H$ is a butterfly minor of a <span class="tooltip">cylindrical directed grid<span class="tooltiptext">Intuitively: concentric cycles directed in the same direction + directed paths between the innermost cycle and the outermost one, in alternating directions. See [\[AKKW16\]](#AKKW16) for a formal definition.</span></span> | any digraph| v | unspecified
[\[CRST17\]](#CRST17) | $\mathcal{M}(H)$, $H$ connected | $\{G, \mathbf{tpw}(G) \leq t\}$  | v/e | $O_{H,t}(k)$
[\[CRST17\]](#CRST17) | $\mathcal{M}(\theta_t)$ | any | v/e | $O_t(k\log k)$
[\[CRST17\]](#CRST17) | <span class="tooltip">$\mathcal{M}(\theta_{t,t'})$<span class="tooltiptext">$\theta_{t,t'}$ is the multigraph obtained from $$\theta_t \cup \theta_{t'}$$ by identifying one vertex of $$\theta_t$$ with one vertex of $$\theta_{t'}$$.</span></span> | simple graphs | e | unspecified
[\[BH17\]](#BH17) | $\mathcal{M}(K_4)$ | any | e | $O(k^8 \log k)$
[\[AFH+17\]](#AFH+17) | <span class="tooltip">$\mathcal{M}(W_t)$<span class="tooltiptext">$W_t$ is the wheel on $t+1$ vertices, i.e. the graph obtained by adding a dominating vertex to a cycle of length $t$.</span></span>, $t \in \mathbb{N}$ | any | v | $O_t(k \log k)$
[\[Ray18\]](#Ray18) | <span class="tooltip">$$\vec{\mathcal{M}}_{s}(H)$$<span class="tooltiptext">$$\vec{\mathcal{M}}_{s}(H)$$ is the class of strong minors of $$H$$, i.e. digraphs that can be obtained from $$H$$ after contracting strongly-connected subgraphs.</span></span> | tournaments | v | unspecified
[\[Ray18\]](#Ray18) | $\vec{\mathcal{M}}_{b}(H)$, $H$ strongly-connected | tournaments | v | unspecified
[\[CHJR18\]](#CHJR18) | $\mathcal{M}(H)$, $H$ planar | any | v | $O_H(k \log k)$
[\[BJS18\]](#BJS18) | $\mathcal{M}(H)$, when $H$ belongs to a <span class="tooltip">proper subclass<span class="tooltiptext">See the paper for the details and the definition of minors in labeled graphs.</span></span> of labelled 2-connected planar graphs | any | v | unspecified
[\[KM19\]](#KM19) | graphs of $\mathcal{M}(H)$ meeting $\geq \ell$ of the prescribed subsets, for $\ell \in \mathbb{N}_{\geq 1}$ and $H$ a planar graph with $\geq \ell-1$ connected components (<span class="tooltip">details<span class="tooltiptext">The host graph $G$ comes with a number of prescribed vertex subsets and the guest graphs are those subgraphs of $G$ that can be constracted to $H$ and intersect $\geq \ell$ of the prescribed subsets. The gap function does not depend on the number of prescribed subsets of the host graph.</span></span>) | any graph with prescribed subsets | v | unspecified

### Topological minors

For every graph $H$, we denote by $\mathcal{T}(H)$ the class of graphs that contain $H$ as a topological minor.
For every digraph $D$, we denote by $\vec{\mathcal{T}}(D)$ the class of all digraphs that contain $D$ as a directed topological minors.

Let $H$ be a graph of maximum degree 3. As a graph $G$ contains $H$ as a minor iff it contains it as a topological minor, all the results stated in the previous section hold for topological minors if the guest graph has maximum degreee 3 (for instance $\mathcal{T}(H)$ has the Erdős-Pósa property for every planar $H$ with maximum degree 3, by [\[RS86\]](#RS86)). These results are not restated here.

|Ref.    |Guest class    |Host class    |T.    |Gap at most |Remark
|---     |            ---|--------------|------|---         |---
[\[Tho88\]](#Tho88) | <span class="tooltip">$$\mathcal{T}_{(0 \mod t)}(H)$$<span class="tooltiptext">$$\mathcal{T}_{(0 \mod t)}(H)$$ is the class of subdivisions of $H$ where every edge is subdivided into a path on $0 \mod t$ edges.</span></span>, $H$ connected planar subcubic | any | v | unspecified
from [\[CC13\]](#CC13) | <span class="tooltip">$$\mathcal{T}_{(0 \mod t)}(H)$$<span class="tooltiptext">$$\mathcal{T}_{(0 \mod t)}(H)$$ is the class of subdivisions of $H$ where every edge is subdivided into a path on $0 \mod t$ edges.</span></span>, $H$ connected planar subcubic | any | v | $O_{H,t}(k~ \mathrm{polylog}~k)$
[\[CHJR18\]](#CHJR18) | <span class="tooltip">$$\mathcal{T}_{(0 \mod t)}(H)$$<span class="tooltiptext">$$\mathcal{T}_{(0 \mod t)}(H)$$ is the class of subdivisions of $H$ where every edge is subdivided into a path on $0 \mod t$ edges.</span></span>, $H$ planar subcubic | any | v | $O_{H,t}(k \log k)$
[\[CHJR18\]](#CHJR18) | <span class="tooltip">$$\mathcal{T}_{(0 \mod t)}(H)$$<span class="tooltiptext">$$\mathcal{T}_{(0 \mod t)}(H)$$ is the class of subdivisions of $H$ where every edge is subdivided into a path on $0 \mod t$ edges.</span></span>, $H$ planar subcubic | any proper minor-closed class $\mathcal{F}$ | v | $O_{H, \mathcal{F},t}(k)$
[\[AKKW16\]](#AKKW16) | $\vec{\mathcal{T}}(H)$, $H$ is a topological minor of a <span class="tooltip">cylindrical directed grid<span class="tooltiptext">Intuitively: concentric cycles directed in the same direction + directed paths between the innermost cycle and the outermost one, in alternating directions. See [\[AKKW16\]](#AKKW16) for a formal definition.</span></span> | any digraph| v | unspecified
[\[Liu17\]](#Liu17) | $\mathcal{T}(H)$ | any | v<sub>1/2</sub> | unspecified | holds for rooted subdivisions
[\[Ray18\]](#Ray18) | $\vec{\mathcal{T}}(H)$, $H$ strongly-connected | tournaments | v | unspecified
[\[BP21\]](#BP21) | $\vec{\mathcal{T}}(H)$ | tournaments | v | $O_H(k \log k)$

### Immersions

For every graph $H$, we denote by $\mathcal{I}(H)$ the class of graphs that contain $H$ as an immersion.

|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |            ---|--------------|------|---
[\[Liu15\]](#Liu15) | $\mathcal{I}(H)$ | 4-edge-connected | e | unspecified
[\[Liu15\]](#Liu15) | <span class="tooltip">$$\mathcal{I}_{1/2}(H)$$<span class="tooltiptext">A graph $H$ is a _half-integral immersion_ of a graph $G$ is $H$ is an immersion of the graph obtained by $G$  after duplicating the multiplicity of all its edges and $$\mathcal{I}_{1/2}(H)$$ denotes the class of all graphs containing $H$ as a half-integral immersion.</span></span> | any | e<sub>1/2</sub> | unspecified
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, $H$ planar subcubic connected on $h>0$ edges | any | e | $\mathrm{poly}(h \cdot k)$
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, $H$ connected  on $h>0$ edges | $\{G, \mathbf{tcw}(G) \leq t\}$ | e | $h \cdot t^2 \cdot k$
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, $H$ connected  on $h>0$ edges | $\{G, \mathbf{tpw}(G) \leq t\}$ | e | $h \cdot t^2 \cdot k$
[\[KK18a\]](#KK18a) | $\mathcal{I}(H)$ | 4-edge-connected | e | unspecified
[\[Ray18\]](#Ray18) | $\vec{\mathcal{I}}(H)$, $H$ strongly-connected | tournaments | e | unspecified
[\[BP21\]](#BP21) | $\vec{\mathcal{I}}(H)$ | tournaments | e | $O_H(k^3)$


### Induced patterns

In this setting, a _$\mathcal{H}$-vertex-packing_ (_$\mathcal{H}$-edge-packing_) in $G$ is a collection of vertex-disjoint (edge-disjoint) **induced** subgraphs of $G$.

|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |            ---|--------------|------|---
[\[EP65\]](#EP65) | cycles of length $\geq 3$ | any | v | $O(k\log k)$
[\[KK18b\]](#KK18b) | cycles of length $\geq 4$ | any | v | $O(k^2\log k)$
trivial | $\mathcal{T}(H)$, $H$ linear forest | any | v | $O(k)$
[\[ALM+18\]](#ALM+18) | {cycles of length $\geq 4$} $\cup$ {asteroidal triples}| any | v | $O(k^2 \log k)$
[\[KR18\]](#KR18) | $\mathcal{T}(H)$, $H\in \\{\text{diamond}, \text{1-pan}, \text{2-pan}\\}$ | any | v | $\mathrm{poly}(k)$
[\[Wei18\]](#Wei18)| cycles of length $\geq t$ for $t \geq 3$| $K_{s,s}$-subgraph free graphs | v | unspecified


### Patterns with prescribed positions

The setting of the results in this section is slightly different: instead of packing/covering all possible occurences of a given pattern in a host graph, we focus on a given list of possible occurences. For instance, one can consider a family $\mathcal{F}$ of (non necessarily disjoint) subtrees of a tree $T$, and compare the maximum number of disjoint elements in $\mathcal{F}$ with the minimum number
	of vertices/edges of $T$ intersecting all elements of $\mathcal{F}$. This is stressed in the following table by refering to guest graphs with words related to substructures (like "subtrees"). Note that there may be two isomorphic subtrees $F,F'$ of $T$ such that $F \in \mathcal{F}$ and $F' \not \in \mathcal{F}$.

For every positive integer $t$, a _$t$-path_ is a disjoint union of $t$
paths, and a _$t$-subpath_ of a $t$-path $G$ is a subgraph that has a
connected intersection with every connected component of $G.$ The
concepts of _$t$-forests_ and _$t$-subforests_ are defined similarly.
We denote by $\textbf{cc}(G)$ the number of connected components of the graph $G$.

|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |            ---|--------------|------|---
[\[HS58\]](#HS58) | subpaths | paths | v | $k$
[\[GL69\]](#GL69) | $t$-subpaths | $t$-paths | v | $O(k^{t!})$
[\[GL69\]](#GL69) | subgraphs $H$ with $\textbf{cc}(H) \leq t$ | paths | v |unspecified
[\[GL69\]](#GL69) | $t$-subforests | $t$-forests | v | unspecified
[\[GL69\]](#GL69) | subtrees | trees | v | $k$
[\[Kai97\]](#Kai97) | $t$-subpaths | $t$-paths | v | $(t^2-t+1)k$
[\[Alo98\]](#Alo98) | $t$-subpaths | $t$-paths | v | $2t^2k$
[\[Alo02\]](#Alo02) | subgraphs $F$ with $\textbf{cc}(F) \leq t$ | trees | v | $2t^2k$
[\[Alo02\]](#Alo02) | subgraphs $H$ with $\textbf{cc}(H) \leq t$ | $\{G, \textbf{tw}(G)\leq w\}$ | v | $2(w+1)t^2k$
[\[Tuz94\]](#Tuz94) | directed subtriangles | planar oriented graphs | e | $k$

### Classes with bounded parameters

Some other results on classes with bounded parameters appear in other tables.

|Ref.    |Guest class    |Host class    |T.    |Gap at most
|---     |            ---|--------------|------|---
[\[RS86\]](#RS86) | $\mathcal{M}(H)$, $H$ planar with $q$ connected components| $\{G, \mathbf{tw}(G)\leq t\}$ | v | $(t-1)(kq-1)$
[\[Tho88\]](#Tho88) | any family of connected graphs | $\{G, \mathbf{tw}(G)\leq t\}$ | v | $k(t+1)$
[\[FJW13\]](#FJW13) | $\{H, \textbf{pw}(H) \geq t\}$ | any | v | $O_t(k)$
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, $H$ connected  on $h>0$ edges | $\{G, \mathbf{tcw}(G) \leq t\}$ | e | $h \cdot t^2 \cdot k$
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, $H$ connected  on $h>0$ edges | $\{G, \mathbf{tpw}(G) \leq t\}$ | e | $h \cdot t^2 \cdot k$
[\[CRST17\]](#CRST17) | any finite family of connected graphs | $\{G, \textbf{tpw}(G) \leq t\}$ | v/e |  $O_{t}(k)$
[\[MMP+19\]](#MMP+19) | all digraphs with directed treewidth $\geq t$ | any digraph | v<sub>1/4</sub> | $\textrm{poly}(k,t)$

### Fractional packings

The results in this table relate the value of the (usual) packing / covering numbers with fractional packing / covering numbers (see the referenced papers for a definition).

|Ref.    |Guest class    |Host class    |$\nu^*$    |$\tau$ |Relationship
|---     |            ---|--------------|-------|-------|---
[\[Sey95\]](#Sey95) | directed cycles | any digraph | max fractional vertex packing| min integral vertex covering | $\tau \leq 4 \nu^* \ln(4 \nu^* ) \ln\log(4\nu^*)$

## Negative results

### Lower bounds for paths

|Ref.    |Guest class    |Host class    |T.    |Gap at least
|---     |            ---|--------------|------|---
[\[MW15\]](#MW15) | $H$-valid paths, $H$ with no matching of size $t$ | all graphs | v | unavoidable dependency in $t$
[\[BHJ18a\]](#BHJ18a) | $S$-paths of length $p \mod t$, for $t>4$ composite and fixed $p \in \\{0, \dots, t-1\\}$ | all graphs | v/e | arbitrary
[\[BU18\]](#BU18) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $1 \mod 4$ | all graphs | v | arbitrary
[\[BU18\]](#BU18) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $3 \mod 4$ | all graphs | v | arbitrary
|[\[BHJ18a\]](#BHJ18a) | odd/even <span class="tooltip">$S$-$T$-paths<span class="tooltiptext">$S,T\subseteq V(G)$ and a _$S$-$T$-path_ is a path with an endpoint in $S$ and the other one in $T$.</span></span> | all graphs | v/e | arbitrary
|[\[BHJ18a\]](#BHJ18a) | odd/even <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> | all graphs | e | arbitrary
[\[BHJ18a\]](#BHJ18a) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $0 \mod 4$ | all graphs | e | arbitrary
[\[BHJ18a\]](#BHJ18a) |  <span class="tooltip">$S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a _$S$-path_ is a path with both endpoints in $S$</span></span> of length $0 \mod p$, for any prime $p$ | all graphs | e | arbitrary
[\[IS17\]](#IS17) | <span class="tooltip">odd $(u,v)$-trails<span class="tooltiptext">An _$(u,v)$-trail_ is a walk from $u$ to $v$ that may have repeated vertices but no repeated edge. A trail is odd if it has an odd number of edges.</span></span> | all graphs | e | $\geq 2k+1$
|[\[BHJ18a\]](#BHJ18a) | <span class="tooltip">$S$-$T$-$S$-paths<span class="tooltiptext">$S.T \subseteq V(G)$. A _$S$-$T$-$S$-path_ is a $S$-path that contains vertices of $T$.</span></span> | all graphs | e | arbitrary
|[\[BHJ18a\]](#BHJ18a) | <span class="tooltip">directed $S$-$T$-$S$-paths<span class="tooltiptext">$S.T \subseteq V(G)$. A _directed $S$-$T$-$S$-path_ is a directed $S$-path that contains vertices of $T$.</span></span> | all digraphs | v/e | arbitrary
|[\[BHJ18a\]](#BHJ18a) | odd/even <span class="tooltip">directed $S$-paths<span class="tooltiptext">$S\subseteq V(G)$ and a directed _$S$-path_ is a directed path with both endpoints in $S$.</span></span> | all digraphs | v/e | arbitrary


### Lower bounds for cycles

|Ref.    |Guest class    |Host class    |T.    |Gap at least|Remark
|---     |            ---|--------------|------|---         |--
[\[Tuz90\]](#Tuz90) | triangles | all graphs | e | $\geq 2k$
[\[Tuz90\]](#Tuz90) | directed triangles | directed graphs without 3 edge-disjoint directed triangles | e | 3
[\[Lov65\]](#Lov65) | cycles | graphs without 2 vertex-disjoint cycles | v | =3
[\[Vos69\]](#Vos69) | cycles | graphs without 3 vertex-disjoint cycles | v | =6
[\[Vos67\]](#Vos67) | cycles | graphs without 4 vertex-disjoint cycles | v | $\geq 9$ | see [\[Vos69\]](#Vos69)
[\[EP65\]](#EP65) | cycles | all graphs | v | $\Omega(k \log k)$
[\[Sim67\]](#Sim67) | cycles | all graphs| v | $> \left (\frac{1}{2} + o(1) \right ) k \log k$
[\[Vos69\]](#Vos69) | cycles | all graphs | v | $\geq \frac{1}{8} k \log k$ | see [\[Vos68\]](#Vos68)
[\[KLL02\]](#KLL02) | cycles | planar graphs | v | $\geq2k$ 
[\[MYZ13\]](#MYZ13) | cycles | planar graphs | e | $\geq 4k-c$, $c\in \mathbb{R}$
[\[DNL87\]](#DNL87) [\[Ree99\]](#Ree99) | odd cycles | all graphs | v | arbitrary
[\[DNL87\]](#DNL87)  | cycles of length $p \mod t$, for every fixed $p \in \\{1, \dots, t-1\\}$ | all graphs | v | arbitrary
[\[Ree99\]](#Ree99) | odd cycles | all graphs | e | arbitrary 
[\[Tho01\]](#Tho01) | odd cycles | planar graphs | v | $\geq 2k$ 
[\[KV04\]](#KV04) | odd cycles | planar graphs | e | $\geq 2k$
[\[PW12\]](#PW12) | $S$-cycles | all graphs | v | $\Omega(k\log k)$
[\[KKL19\]](#KKL19) | $S$-cycles | graphs without two disjoint $S$-cycles | v | =4
[\[KK13\]](#KK13) | odd $S$-cycles | all graphs | v | arbitrary
[\[Bru19\]](#Bru19) | even $S$-cycles of length $\geq \ell$, for every fixed $\ell \geq 5$ | all graphs | e | arbitrary
[\[Bru19\]](#Bru19) | $S$-cycles of length $0 \mod t$, for every fixed $t > 2$ | all graphs | e | arbitrary
[\[Sey95\]](#Sey95) | directed cycles | all digraphs | <span class="tooltip">v<span class="tooltiptext">Actually proved for fractional packings</span></span> | $\frac{1}{30} k \ln k$
[\[GW96\]](#GW96) | directed cycles | planar digraphs | v | $\frac{3}{2}k$ | simpler: a $C_5$ where every edge is replaced by a directed triangle
[\[KK12\]](#KK12) | directed $S$-cycles | all digraphs | v/e | arbitrary
[\[KKKK13\]](#KKKK13) | odd directed $S$-cycles | all digraphs | v | arbitrary
[\[Bir03\]](#Bir03) | cycles of length $\geq 4$ | graphs without 2 vertex-disjoint cycles of length $\geq 4$ | v | =4
[\[Bir03\]](#Bir03) | cycles of length $\geq 5$ | graphs without 2 vertex-disjoint cycles of length $\geq 5$ | v | =5
[\[FH14\]](#FH14) | cycles of length $\geq t$ | all graphs | v | $\Omega(k\log k)$, $t$ fixed
[\[FH14\]](#FH14) | cycles of length $\geq t$ | all graphs | v | $\Omega(t)$, $k$ fixed 
[\[MNŠW17\]](#MNŠW17) | cycles of length $\geq t$ | all graphs | v | $\geq(k-1)t$
[\[MNŠW17\]](#MNŠW17) | cycles of length $\geq t$ | all graphs | v | $\geq \frac{(k-1)\log k}{8}$
[\[BHJ16\]](#BHJ16) | cycles of length $\geq t$ | all graphs | e | $O(k \log k + kt)$
[\[Sim67\]](#Sim67) | <span class="tooltip">dumb-bells<span class="tooltiptext">A _dumb-bell_ is a graph obtained by connecting two cycles by a (non-trivial) path.</span></span> | all graphs | v | $>(1+o(1))k \log k$ 

### Lower bounds for minors

Notice that if $H$ is a subcubic graph, then any negative result about the Erdős-Pósa property of $\mathcal{T}(H)$ implies the same for $\mathcal{M}(H)$.
Therefore the table below should be completed with the negative results for subcubic $H$'s that are presented in the following section, in particular those of [\[BHJ18b\]](#BHJ18b).

|Ref.    |Guest class    |Host class    |T.    |Gap at least
|---     |            ---|--------------|------|---
<span class="tooltip">\[?\]<span class="tooltiptext">This follows from the existence of $n$-vertex 3-regular graphs with treewidth $\Omega(n)$ and girth $\Omega(\log n)$ (Ramanujan graphs, for instance).</span></span> | $\mathcal{M}(H)$, for every $H$ that has a cycle | all graphs | v/e | $\Omega(k \log k)$
[\[RS86\]](#RS86) | $\mathcal{M}(H)$, for every non-planar $H$ | graphs of same Euler genus as $H$ | v | arbitrary
[\[RT17\]](#RT17) | $\mathcal{M}(H)$, for every non-planar $H$ | graphs of same Euler genus as $H$ | e | arbitrary
[\[AKKW16\]](#AKKW16) | $\vec{\mathcal{M}}(H)$, for every $H$ that is not butterfly-minor of the cylindrical grid | all digraphs | v | arbitrary
[\[BJS18\]](#BJS18) | $\mathcal{M}(H)$, for some labelled $H$ that is not 2-connected <span class="tooltip">(and other cases)<span class="tooltiptext">See the paper for the details and the definition of minors in labelled graphs.</span></span> | all graphs | v | arbitrary
[\[KM19\]](#KM19) | graphs of $\mathcal{M}(H)$ meeting $\geq \ell$ of the prescribed subsets, for $\ell \in \mathbb{N}_{\geq 1}$ and $H$ a connected planar graph with $\leq \ell-2$ connected components (<span class="tooltip">details<span class="tooltiptext">The host graph $G$ comes with a number of prescribed vertex subsets and the guest graphs are those subgraphs of $G$ that can be constracted to $H$ and intersect $\geq \ell$ of the prescribed subsets.</span></span>) | square grids with prescribed subsets | v | arbitrary

### Lower bounds for topological minors

|Ref.    |Guest class    |Host class    |T.    |Gap at least
|---     |            ---|--------------|------|---
[\[Tho88\]](#Tho88) | $\mathcal{T}(H)$, for every planar $H$ that has no plane embedding with all degree-$(\geq 4)$ vertices on the same face | all graphs | v | arbitrary
[\[RT17\]](#RT17) | $\mathcal{T}(H)$, for every $H$ non-planar | graphs of same Euler genus as $H$ | v | arbitrary
[\[Tho88\]](#Tho88) | $\mathcal{T}(H)$, for infinitely many trees $H$ with $\Delta(H)=4$ | planar graphs | e | arbitrary
[\[RT17\]](#RT17) | $\mathcal{T}(H)$, for every $H$ subcubic and non-planar | graphs of same Euler genus as $H$ | e | arbitrary
[\[BHJ18b\]](#BHJ18b) | $\mathcal{T}(\mathrm{Grid}_{2\times k})$, for every $k\geq 71$ | a class of graphs of treewidth $\leq 8$ | e | arbitrary
[\[BHJ18b\]](#BHJ18b) | $\mathcal{T}(H)$, for every subcubic tree $H$ of pathwidth $\geq 19$ | a class of graphs of treewidth $\leq 6$ | e | arbitrary
[\[AKKW16\]](#AKKW16) | $\vec{\mathcal{T}}(H)$, for every $H$ that is not subdivision of the cylindrical wall | all digraphs | v | arbitrary


### Lower bounds for immersions

|Ref.    |Guest class    |Host class    |T.    |Gap at least
|---     |            ---|--------------|------|---
copying [\[Tho88\]](#Tho88) | $\mathcal{I}(H)$, for infinitely many trees $H$ with $\Delta(H)=4$ | planar graphs | e | arbitrary
[\[Liu15\]](#Liu15) | $\mathcal{I}(H)$ <span class="tooltip">for some $H$<span class="tooltiptext">Actually this is not proved in the preprint [\[Liu15\]](#Liu15) but stated as a consequence of a manuscript of the author that is not availlable online. A description of the graphs $H$ such that $\mathcal{I}(H)$ does not have the edge-EP property in 3-edge-connected graphs is not given in [\[Liu15\]](#Liu15).</span></span> | 3-edge-connected graphs | e | arbitrary
[\[RT17\]](#RT17) | $\mathcal{I}(H)$, for every $H$ non-planar | graphs of same Euler genus as $H$ | v | arbitrary
[\[RT17\]](#RT17) | $\mathcal{I}(H)$, for every $H$ subcubic and non-planar| graphs of same Euler genus as $H$ | e | arbitrary
[\[GKRT17\]](#GKRT17) | $\mathcal{I}(H)$, for some 3-connected $H$ with $\Delta(H)=4$ | planar graphs | e | arbitrary
[\[KK18a\]](#KK18a) | $\mathcal{I}(K_5)$ | 3-edge-connected graphs | e | arbitrary

### Lower bounds for induced patterns

|Ref.    |Guest class    |Host class    |T.    |Gap at least
|---     |            ---|--------------|------|---
[\[KK18b\]](#KK18b) | cycles of length $\geq t$, for every $t \geq 5$ | all graphs | v | arbitrary
[\[KR18\]](#KR18) | $\mathcal{T}(K_{n,m})$, for every $n\geq 2$ and $m \geq 3$ | all graphs | v | arbitrary
[\[KR18\]](#KR18) | $\mathcal{T}(F)$, for every $F$ forest where two $(\geq 3)$-degree vertices lie in the same component | all graphs | v | arbitrary
[\[KR18\]](#KR18) | $\mathcal{T}(H)$, for every $H$ that has an induced cycle of length $\geq 5$ | all graphs | v | arbitrary



## References

  * <span id="AFH+17">\[AFH+17\]</span> Pierre Aboulker, Samuel Fiorini, Tony Huynh, Gwenaël Joret, Jean-Florent Raymond, and Ignasi Sau. [A tight Erdős-Pósa function for wheel minors](https://doi.org/10.1137/17M1153169), _SIAM Journal on Discrete Mathematics_, 32(3):2302-2312, 2018.
  * <span id="AKKW16">\[AKKW16\]</span> Saeed Akhoondian Amiri, Ken-ichi Kawarabayashi, Stephan Kreutzer, and Paul Wollan. [The Erdős–Pósa property for directed graphs](https://arxiv.org/abs/1603.02504). _ArXiv preprint arXiv:0904.0727_, 2016.
  * <span id="ALBT11">\[ALBT11\]</span> S. Aparna Lakshmanan, Csilla Bujtás, and Zsolt Tuza. [Small edge sets meeting all triangles of a graph](https://doi.org/10.1007/s00373-011-1048-8). _Graphs and Combinatorics_, 28(3):381--392, 2011.
  * <span id="ALM+18">\[ALM+18\]</span> Akanksha Agrawal, Daniel Lokshtanov, Pranabendu Misra, Saket Saurabh, and Meirav Zehavi. [Erdős-Pósa Property of Obstructions to Interval Graphs](http://dx.doi.org/10.4230/LIPIcs.STACS.2018.7). In _Proceedings of the 35th Symposium on Theoretical Aspects of Computer Science (STACS 2018)_, LIPIcs, Schloss Dagstuhl-Leibniz-Zentrum fuer Informatik, vol. 96, 7:1--7:15, 2018.
  * <span id="Alo98">\[Alo98\]</span> Noga Alon. [Piercing $d$-intervals](http://doi.org/19:333.doi:10.1007/PL00009349). _Discrete & Computational Geometry_, 19(3):333--334, 1998.
  * <span id="Alo02">\[Alo02\]</span> Noga Alon. [Covering a hypergraph of subgraphs](https://doi.org/10.1016/S0012-365X(02)00427-2). _Discrete Mathematics_, 257(2–3):249 -- 254, 2002.
  * <span id="BBG+22">\[BBG+22\]</span> Marthe Bonamy, Łukasz Bożyk, Andrzej Grzesik, Meike Hatzel, Tomáš Masařík, Jana Novotná, and Karolina Okrasa. [Tuza's Conjecture for Threshold Graphs](https://doi.org/10.46298/dmtcs.7660). _Discrete Mathematics & Theoretical Computer Science_, 14(1), 2022.
  * <span id="BBR07">\[BBR07\]</span> Etienne Birmelé, J. Adrian Bondy, and Bruce A. Reed. [The Erdős-Pósa property for long circuits](http://doi.org/doi:10.1007/s00493-007-0047-0). _Combinatorica_, 27(2):135--145, 2007.
  * <span id="BD92">\[BD92\]</span> Daniel Bienstock and Nathaniel Dean. [On obstructions to small face covers in planar graphs](https://doi.org/10.1016/0095-8956(92)90040-5). _Journal of Combinatorial Theory, Series B_, 55(2):163--189, 1992.
  * <span id="BDM+19">\[BDM+19\]</span> Marthe Bonamy, François Dross, Tomáš Masařík, Wojciech Nadara, Marcin Pilipczuk, and Michał Pilipczuk. [Jones’ Conjecture in subcubic graphs](https://arxiv.org/abs/1912.01570). _ArXiv preprint arXiv:1912.01570_, 2019.
  * <span id="BFG21">\[BGF21\]</span> Fábio Botler, Cristina G. Fernandes, and Juan Gutiérrez. [On Tuza’s conjecture for triangulations and graphs with small treewidth](https://doi.org/10.1016/j.disc.2020.112281). _Discrete Mathematics_, 344(4):112281, 2021.
  * <span id="BH17">\[BH17\]</span> Henning Bruhn and Matthias Heinlein. [$K_4$-expansions have the edge-Erdős-Pósa property](https://arxiv.org/abs/1808.10380). In _Proceedings of EuroComb 2017_, Electronic Notes in Discrete Mathematics, 61:163--168, 2017.
  * <span id="BHJ16">\[BHJ16\]</span> Henning Bruhn, Matthias Heinlein, and Felix Joos. [Long cycles have the edge-Erdős-Pósa property](https://doi.org/10.1007/s00493-017-3669-x). _Combinatorica_, 2016.
  * <span id="BHJ18a">\[BHJ18a\]</span> Henning Bruhn, Matthias Heinlein, and Felix Joos. [Frames, A-paths and the Erdős-Pósa property](https://doi.org/10.1137/17M1148542). _SIAM Journal on Discrete Mathematics_, 32(2):1246--1260, 2018.
  * <span id="BHJ18b">\[BHJ18b\]</span> Henning Bruhn, Matthias Heinlein, and Felix Joos. [The edge-Erdős-Pósa property](https://doi.org/10.1007/s00493-020-4071-7). _Combinatorica_, 41(2):147--173, 2021.
  * <span id="Bir03">\[Bir03\]</span> Étienne Birmelé. [Largeur d'arborescence, quasi-clique-mineurs et propriétés d'Erdős-Pósa](https://www.theses.fr/2003LYO10259). PhD Thesis. Université Lyon 1, 2003.
  * <span id="BJS17">\[BJS17\]</span> Henning Bruhn, Felix Joos, and Oliver Schaudt. [Long cycles through prescribed vertices have the Erdős-Pósa property](https://doi.org/10.1002/jgt.22156). _Journal of Graph Theory_, 87:275–284, 2017.
  * <span id="BJS18">\[BJS18\]</span> Henning Bruhn, Felix Joos, and Oliver Schaudt. [Erdős-Pósa property for labelled minors: 2-connected minors](https://doi.org/10.1137/19M1289340). _SIAM Journal on Discrete Mathematics_, 35(2):893-914, 2021.
  * <span id="BP21">\[BP21\]</span> Łukasz Bożyk and Michał Pilipczuk. [On the Erdős-Pósa property for immersions and topological minors in tournaments](https://doi.org/10.46298/dmtcs.7099). _Discrete Mathematics and Theoretical Computer Science_, 24(1), 2022.
  * <span id="BR00">\[BR00\]</span> Claude Berge and Bruce Reed. [Optimal packings of edge-disjoint odd cycles](https://doi.org/10.1016/S0012-365X(99)00283-6). _Discrete Mathematics_, 211(1–3):197 -- 202, 2000.
  * <span id="Bru19">\[Bru19\]</span> Henning Bruhn. [Even A-cycles have the edge-Erdős-Pósa property](https://doi.org/10.1002/jgt.22778). _Journal of Graph Theory_, 100(2):281-293, 2022.
  * <span id="BT15">\[BT15\]</span> Nicolas Bousquet and Stéphan Thomassé. [VC-dimension and Erdős–Pósa property](https://doi.org/10.1016/j.disc.2015.05.026). _Discrete Mathematics_, 338(12):2302 -- 2317, 2015.
  * <span id="BU18">\[BU18\]</span> Henning Bruhn and Arthur Ulmer. [Packing A-paths of length zero modulo four](https://doi.org/10.1016/j.ejc.2021.103422). _European Journal of Combinatorics_, 99:103422, 2022.
  * <span id="CC13">\[CC13\]</span> Chandra Chekuri and Julia Chuzhoy. [Large-treewidth graph decompositions and applications](https://arxiv.org/abs/1304.1577). In _Proceedings of the forty-fifth annual ACM symposium on Theory of computing_, pages 291--300. ACM, 2013.
  * <span id="CC14">\[CC14\]</span> Chandra Chekuri and Julia Chuzhoy. [Polynomial bounds for the grid-minor theorem](https://arxiv.org/abs/1305.6577). In _Proceedings of the 46th Annual ACM Symposium on Theory of Computing_, pages 60--69. ACM, 2014.
  * <span id="CGG+06">\[CGG+06\]</span> Maria Chudnovsky, Jim Geelen, Bert Gerards, Luis Goddyn, Michael Lohman, and Paul Seymour. [Packing non-zero $A$-paths in group-labelled graphs](http://doi.org/10.1007/s00493-006-0030-1). _Combinatorica_, 26(5):521--532, 2006.
  * <span id="CGH14">\[CGH14\]</span> Glenn G. Chappell, John Gimbel, and Chris Hartmann. [On cycle packings and feedback vertex sets](http://cdm.ucalgary.ca/cdm/index.php/cdm/article/view/318/185). _Contributions to Discrete Mathematics_, 9(2), 2014.
  * <span id="CHC12">\[CHC12\]</span> Hong-Bin Chen, Fu Hung-Lin, and Shih Chie-Huai. [Feedback vertex set on planar graphs](https://doi.org/doi:10.11650/twjm/1500406840). _Taiwanese Journal of Mathematics_, 16.6:2077-2082,2012.
  * <span id="CHJR18">\[CHJR18\]</span> Wouter Cames van Batenburg, Tony Huynh, Gwenaël Joret, and Jean-Florent Raymond. [A tight Erdős-Pósa function for planar minors](https://doi.org/10.1137/17M1153169). _Advances in Combinatorics_, 2019:2, 2019.
  * <span id="CJU19">\[CJU19\]</span> Wouter Cames van Batenburg, Gwenaël Joret, and Arthur Ulmer. [Erdős-Pósa from ball packing](https://doi.org/10.1137/19M1309225). _SIAM Journal on Discrete Mathematics_, 34(3):1609-1619, 2020.
  * <span id="CRST17">\[CRST17\]</span> Dimitris Chatzidimitriou, Jean-Florent Raymond, Ignasi Sau, and Dimitrios M. Thilikos. [An $O( \log \rm {OPT})$-approximation for covering/packing minor models of $\theta_r$](http://doi.org/10.1007/s00453-017-0313-5). _Algorithmica_, 2018, 80(4):1330--1356.
  * <span id="Die05">\[Die05\]</span> Reinhard Diestel. [Graph Theory](http://diestel-graph-theory.com), volume 173 of _Graduate Texts in Mathematics_. Springer-Verlag, Heidelberg, third edition, 2005.
  * <span id="DKW12">\[DKW12\]</span> Reinhard Diestel, Ken-ichi Kawarabayashi, and Paul Wollan. [The Erdős--Pósa property for clique minors in highly connected graphs](https://doi.org/10.1016/j.jctb.2011.08.001). _Journal of Combinatorial Theory, Series B_, 102(2):454--469, 2012.
  * <span id="DNL87">\[DNL87\]</span> IJ Dejter and V Neumann-Lara. [Unboundedness for generalized odd cyclic transversality](https://scholar.google.de/scholar?q=Unboundedness+for+generalized+odd+cyclic+transversality). In _Combinatorics (Eger, 1987), Colloq. Math. Soc. János Bolyai_, volume 52, pages 195--203, 1987.
  * <span id="DO96">\[DO96\]</span> Guoli Ding and Bogdan Oporowski. [On tree-partitions of graphs](https://doi.org/10.1016/0012-365X(94)00337-I). _Discrete Mathematics_, 149(1--3):45 -- 58, 1996.
  * <span id="DXZ03">\[DXZ03\]</span> Guoli Ding, Zhenzhen Xu, and Wenan Zang. [Packing cycles in graphs, II](https://doi.org/10.1016/S0095-8956(02)00007-2). _Journal of Combinatorial Theory, Series B_, 87(2):244--253, 2003.
  * <span id="DZ02">\[DZ02\]</span> Guoli Ding and Wenan Zang. [Packing cycles in graphs](https://doi.org/10.1006/jctb.2002.2134). _Journal of Combinatorial Theory, Series B_, 86(2):381 -- 407, 2002.
  * <span id="EP65">\[EP65\]</span> Paul Erdős and Louis Pósa. [On independent circuits contained in a graph](http://ftp.bolyai.hu/~p_erdos/1965-05.pdf). _Canadian Journal of Mathematics_, 17:347--352, 1965.
  * <span id="FH14">\[FH14\]</span> Samuel Fiorini and Audrey Herinckx. [A tighter Erdős-Pósa function for long cycles](http://doi.org/10.1002/jgt.21776). _Journal of Graph Theory_, 77(2):111--116, 2014.
  * <span id="FHRV06">\[FHRV06\]</span> Samuel Fiorini, Nadia Hardy, Bruce Reed, and Adrian Vetta. [Approximate min-max relations for odd cycles in planar graphs](https://doi.org/10.1007/s10107-006-0063-7). _Mathematical Programming_, 110(1):71-91. Springer, 2006.
  * <span id="FJW13">\[FJW13\]</span> Samuel Fiorini, Gwenaël Joret, and David R. Wood. [Excluded forest minors and the Erdős-Pósa property](https://doi.org/10.1017/S0963548313000266). _Combinatorics, Probability & Computing_, 22(5):700--721, 2013.
  * <span id="FLM+13">\[FLM+13\]</span> Fedor V Fomin, Daniel Lokshtanov, Neeldhara Misra, Geevarghese Philip, and Saket Saurabh. [Quadratic upper bounds on the Erdős--pósa property for a generalization of packing and covering cycles](http://doi.org/10.1002/jgt.21720). _Journal of Graph Theory_, 74(4):417--424, 2013.
  * <span id="FS13">\[FS13\]</span> Alexandra Fradkin and Paul Seymour. [Tournament pathwidth and topological containment](https://doi.org/10.1016/j.jctb.2013.03.001). _Journal of Combinatorial Theory, Series B_, 103(3):374 -- 384, 2013.
  * <span id="FST11">\[FST11\]</span> Fedor V. Fomin, Saket Saurabh, and Dimitrios M. Thilikos. [Strengthening Erdős--Pósa property for minor-closed graph classes](http://doi.org/10.1002/jgt.20503). _Journal of Graph Theory_, 66(3):235--240, 2011.
  * <span id="Gal64">\[Gal64\]</span> Tibor Gallai. [Maximum-minimum sätze und verallgemeinerte faktoren von graphen](http://doi.org/10.1007/BF02066678). _Acta Mathematica Hungarica_, 12, 03 1964.
  * <span id="GGR+09">\[GGR+09\]</span> Jim Geelen, Bert Gerards, Bruce Reed, Paul Seymour, and Adrian Vetta. [On the odd-minor variant of Hadwiger's conjecture](https://doi.org/10.1016/j.jctb.2008.03.006). _Journal of Combinatorial Theory, Series B_ 99(1):20--29, 2009.
  * <span id="GHK+21">\[GHK+21\]</span> Pascal Gollin, Kevin Hendrey, Ken-ichi Kawarabayashi, O-joug Kwon, and Sang-il Oum. [A unified half-integral Erdős-Pósa theorem for cycles in graphs labelled by multiple abelian groups](https://arxiv.org/abs/2102.01986). arXiv preprint arXiv:2102.01986. January 2021.
  * <span id="GKRT17">\[GKRT17\]</span> Archontia Giannopoulou, O-joung Kwon, Jean-Florent Raymond, and Dimitrios M. Thilikos. [Packing and covering immersion models of planar subcubic graphs](https://doi.org/10.1016/j.ejc.2017.05.009). _European Journal of Combinatorics_, 65:154--167, 2017.
  * <span id="GL69">\[GL69\]</span> András Gyárfás and Jenö Lehel. [A Helly-type problem in trees](http://www.renyi.hu/~gyarfas/Cikkek/02_GyarfasLehel_AHellyTypeProblemInTrees.pdf). In _Combinatorial Theory and its applications_, pages 571--584. Colloquia Mathematica Societatis J\'anos Bolyai, 1969.
  * <span id="GR09">\[GR09\]</span> Alexanders Grigoriev and René Sitters. [Connected feedback vertex set in planar graphs](https://doi.org/10.1007/978-3-642-11409-0_13). In _International Workshop on Graph-Theoretic Concepts in Computer Science (WG 2009)_, volume 5911 of _LNCS_, pages 143--153. Springer, Berlin, Heidelberg, 2009.
  * <span id="Grü38">\[Grü38\]</span> Tibor Grünwald. [Ein neuer beweis eines mengerschen satzes](http://doi.org/10.1112/jlms/s1-13.3.188). _Journal of the London Mathematical Society_, 1(3):188--192, 1938.  
  * <span id="GT11">\[GT11\]</span> Bertrand Guenin and Robin Thomas. [Packing directed circuits exactly](http://doi.org/10.1007/s00493-011-1687-5). _Combinatorica_, 31(4):397--421, 2011.
  * <span id="GV95">\[GV95\]</span> Yubao Guo and Lutz Volkmann. [A generalization of Menger's theorem for certain block-cactus graphs](https://doi.org/10.1007/BF01787420). _Graphs and Combinatorics_, 11(1):49--52, 1995.
  * <span id="GW96">\[GW96\]</span> Michel Goemans and David Williamson. [Primal-dual approximation algorithms for feedback problems in planar graphs](http://doi.org/10.1007/PL00009810). _Combinatorica_, 18:37--39, 1998.
  * <span id="Hax99">\[Hax99\]</span> Penny Haxell. [Packing and covering triangles in graphs](https://doi.org/10.1016/S0012-365X(98)00183-6). _Discrete Mathematics_, 195(1–3):251 -- 254, 1999.
  * <span id="HJW18">\[HJW18\]</span> Tony Huynh, Felix Joos, and Paul Wollan. [A unified Erdős--Pósa theorem for constrained cycles](https://doi.org/10.1007/s00493-017-3683-z). _Combinatorica_, 2018.
  * <span id="HK88">\[HK88\]</span> Penny Haxell and Y. Kohayakawa. [Packing and covering triangles in tripartite graphs](http://doi.org/10.1007/s003730050010). _Graphs and Combinatorics_, 14(1):1--10, 1988.
  * <span id="HKT11">\[HKT11\]</span> Penny Haxell, Alexandr Kostochka, and Stéphan Thomassé. [Packing and covering triangles in $K_4$-free planar graphs](http://doi.org/10.1007/s00373-011-1071-9). _Graphs and Combinatorics_, 28(5):653--662, 2011.
  * <span id="HM13">\[HM13\]</span> Frédéric Havet and Ana Karolinna Maia. [On disjoint directed cycles with prescribed minimum lengths](https://hal.archives-ouvertes.fr/hal-00816135/). Research Report RR-8286, INRIA, April 2013.
  * <span id="HS58">\[HS58\]</span> András Hajnal and János Surányi. [Über die auflösung von graphen in vollständige teilgraphen](https://scholar.google.fr/scholar?q=%C3%9Cber+die+aufl%C3%B6sung+von+graphen+in+vollstandige+teilgraphen&btnG=&hl=fr&as_sdt=0%2C5). _Annales Universitatis Scientarium Budapestinensis de Rolando Eötvös Nominatae, Sectio Mathematica_, 1:113--121, 1958.
  * <span id="HU19">\[HU19\]</span> Matthias Heinlein and Arthur Ulmer. [Long $A$-$B$-paths have the edge-Erdős-Pósa property](https://arxiv.org/abs/1903.07989). arXiv preprint arXiv:1903.07989, 2019.
  * <span id="IS17">\[IS17\]</span> Sharat Ibrahimpur and Chaitanya Swamy. [Min-Max Theorems for Packing and Covering Odd $(u,v)$-trails](https://doi.org/10.1007/978-3-319-59250-3_23). In _Integer Programming and Combinatorial Optimization: 19th International Conference, IPCO 2017, Waterloo, ON, Canada, June 26-28, 2017, Proceedings_, pages 279--291, 2017.
  * <span id="Joo14">\[Joo14\]</span> Felix Joos. [Parity linkage and the Erdős-Pósa property of odd cycles through prescribed vertices in highly connected graphs](http://doi.org/10.1002/jgt.22103). _Journal of Graph Theory_, to appear in 2017.
  * <span id="JRST01">\[JRST01\]</span> Thor Johnson, Neil Robertson, Paul D. Seymour, and Robin Thomas. [Directed tree-width](https://doi.org/10.1006/jctb.2000.2031). _Journal of Combinatorial Theory, Series B_, 82(1):138 -- 154, 2001.
  * <span id="Kai97">\[Kai97\]</span> Tomás Kaiser. [Transversals of $d$-intervals](http://doi.org/10.1007/PL00009315). _Discrete & Computational Geometry_, 18(2):195--203, 1997.
  * <span id="KK12">\[KK12\]</span> Naonori Kakimura and Ken-ichi Kawarabayashi. [Packing directed circuits through prescribed vertices bounded fractionally](http://doi.org/10.1137/100786423). _SIAM Journal on Discrete Mathematics_, 26(3):1121--1133, 2012.
  * <span id="KK13">\[KK13\]</span> Naonori Kakimura and Ken-ichi Kawarabayashi. [Half-integral packing of odd cycles through prescribed vertices](https://doi.org/10.1007/s00493-013-2865-6). _Combinatorica_, 33(5):549--572, 2013.
  * <span id="KK15">\[KK15\]</span> Naonori Kakimura and Ken-ichi Kawarabayashi. [Fixed-parameter tractability for subset feedback set problems with parity constraints](https://doi.org/10.1016/j.tcs.2015.02.004). _Theoretical Computer Science_, 576:61--76, 2015.
  * <span id="KK16">\[KK16\]</span> Ken-ichi Kawarabayashi and Yusuke Kobayashi. [Edge-disjoint odd cycles in 4-edge-connected graphs](https://doi.org/10.1016/j.jctb.2015.12.002). _Journal of Combinatorial Theory, Series B_, 119:12--27, 2016.
  * <span id="KK18a">\[KK18a\]</span> Naonori Kakimura and Ken-ichi Kawarabayashi. [The Erdős–Pósa property for edge-disjoint immersions in 4-edge-connected graphs](https://doi.org/10.1016/j.jctb.2018.02.003). _Journal of Combinatorial Theory, Series B_, 131:138--169, 2018.
  * <span id="KK18b">\[KK18b\]</span> Eun Jung Kim and O-joung Kwon. [Erdős-Pósa property of chordless cycles and its applications](https://doi.org/10.1137/1.9781611975031.109). _Proceedings of the Twenty-Ninth Annual ACM-SIAM Symposium on Discrete Algorithms_. Society for Industrial and Applied Mathematics, 2018.
  * <span id="KKKK13">\[KKKK13\]</span> Ken-ichi Kawarabayashi, Daniel Král, Marek Krčál, and Stephan Kreutzer. [Packing directed cycles through a specified vertex set](http://dl.acm.org/citation.cfm?id=2627817.2627844). In _Proceedings of the Twenty-Fourth Annual ACM-SIAM Symposium on Discrete Algorithms_, pages 365--377. Society for Industrial and Applied Mathematics, 2013.
  * <span id="KKKX20">\[KKKX20\]</span> Ken-ichi Kawarabayashi, Stephan Kreutzer, O-joung Kwon, and Qiqin Xie. [Half-integral Erdős-Pósa property of directed odd cycles](https://arxiv.org/abs/2007.12257). arXiv preprint arXiv:2007.12257, 2020. 
  * <span id="KKL19">\[KKL19\]</span> Minjeong Kang, O-joung Kwon, and Myounghwan Lee. [Graphs without two vertex-disjoint S-cycles](https://doi.org/10.1016/j.disc.2020.111997). _Discrete Mathematics_, 343(10):111997, 2020.
  * <span id="KKM11">\[KKM11\]</span> Naonori Kakimura, Ken-ichi Kawarabayashi, and Dániel Marx. [Packing cycles through prescribed vertices](https://doi.org/10.1016/j.jctb.2011.03.004). _Journal of Combinatorial Theory, Series B_, 101(5):378 -- 381, 2011.
  * <span id="KLL02">\[KLL02\]</span> Tom Kloks, Chuan-Min Lee, and Jiping Liu. [New algorithms for $k$-face cover, $k$-feedback vertex set, and $k$-disjoint cycles on plane and planar graphs](https://doi.org/10.1007/3-540-36379-3_25). In _28th International Workshop on Graph Theoretic Concepts in Computer Science (WG 2002)_, volume 2573 of _LNCS_, pages 282--295. Springer, Berlin, 2002.
  * <span id="KM19">\[KM19\]</span> O-joung Kwon and Dániel Marx. [Erdős-Pósa property of minor-models with prescribed vertex sets](https://arxiv.org/abs/1904.00879). arXiv preprint arXiv:1904.00879, 2019.
  * <span id="KN07">\[KN07\]</span> Ken-ichi Kawarabayashi and Atsuhiro Nakamoto. [The Erdős–Pósa property for vertex- and edge-disjoint odd cycles in graphs on orientable surfaces](https://doi.org/10.1016/j.disc.2006.07.008). _Discrete Mathematics_, 307(6):764 -- 768, 2007.
  * <span id="Kőn31">\[Kőn31\]</span> Denés Kőnig. [Gráfok és mátrixok](https://en.wikipedia.org/wiki/K%C5%91nig%27s_theorem_(graph_theory)). _Matematikai és Fizikai Lapok_, 38:116–119. 1931.
  * <span id="KR09">\[KR09\]</span> Ken-ichi Kawarabayashi and Bruce Reed. [Highly parity linked graphs](https://link.springer.com/article/10.1007%2Fs00493-009-2178-y). _Combinatorica_, 29(2):215--225, 2009.
  * <span id="KR18">\[KR18\]</span> O-joung Kwon and Jean-Florent Raymond. [Packing and covering induced subdivisions](https://doi.org/10.1137/18M1226166). _SIAM Journal on Discrete Mathematics_, 35(2):597-636, 2021.
  * <span id="Kri95">\[Kri95\]</span> Michael Krivelevich. [On a conjecture of Tuza about packing and covering of triangles](https://doi.org/10.1016/0012-365X(93)00228-W). _Discrete Mathematics_, 142(1–3):281 -- 286, 1995.
  * <span id="KSS12">\[KSS12\]</span> Daniel Král', Jean-Sébastien Sereni, and Ladislav Stacho. [Min-Max Relations for Odd Cycles in Planar Graphs](https://doi.org/10.1137/110845835). _SIAM Journal on Discrete Mathematics_, 26(3):884-895, 2012. 
  * <span id="KV04">\[KV04\]</span> Daniel Král’ and Heinz-Jürgen Voss. [Edge-disjoint odd cycles in planar graphs](https://doi.org/10.1016/S0095-8956(03)00078-9). _Journal of Combinatorial Theory, Series B_, 90(1):107 -- 120, 2004.
  * <span id="KW06">\[KW06\]</span> Ken-ichi Kawarabayashi and Paul Wollan. [Non-zero disjoint cycles in highly connected group labelled graphs](https://doi.org/10.1016/j.jctb.2005.08.001). _Journal of Combinatorial Theory, Series B_, 96(2):296--301, 2006.
  * <span id="Liu15">\[Liu15\]</span> Chun-Hung Liu. [Packing and Covering Immersions in 4-Edge-Connected Graphs](https://doi.org/10.1016/j.jctb.2021.06.005). _Journal of Combinatorial Theory, Series B_, 151:148--222, 2021.
  * <span id="Liu17">\[Liu17\]</span> Chun-Hung Liu. [Packing topological minors half-integrally](https://arxiv.org/abs/1707.07221). _ArXiv preprint arXiv:1707.07221_, July 2017.
  * <span id="Lov65">\[Lov65\]</span> László Lovász. On graphs not containing independent circuits. (Hungarian) _Matematikai Lapok_, 16(7):289-299, 1965.
  * <span id="Lov76">\[Lov76\]</span> László Lovász. [On two minimax theorems in graph](https://doi.org/10.1016/0095-8956(76)90049-6). _Journal of Combinatorial Theory, Series B_, 21(2):96--103, 1976.
  * <span id="LBT16">\[LBT16\]</span> Aparna Lakshmanan S., Csilla Bujtás, and Zsolt Tuza. [Induced cycles in triangle graphs](https://doi.org/10.1016/j.dam.2015.12.012). _Discrete Applied Mathematics_, 209:264--275, 2016. 
  * <span id="LY78">\[LY78\]</span> Claudio L. Lucchesi and D. H. Younger. [A minimax theorem for directed graphs](http://doi.org/10.1112/jlms/s2-17.3.369). _Journal of the London Mathematical Society (2)_, 17(3):369--374, 1978.
  * <span id="Mad78a">\[Mad78a\]</span> Wolfgang Mader. [Über die maximalzahl kantendisjunkter $A$-wege](http://doi.org/10.1007/BF01226062). _Archiv der Mathematik_, 30(1):325--336, 1978.
  * <span id="Mad78b">\[Mad78b\]</span> Wolfgang Mader. [Über die maximalzahl kreuzungsfreier $H$-wege](http://doi.org/10.1007/BF01226465). _Archiv der Mathematik_, 31(1):387--402, 1978.
  * <span id="Men27">\[Men27\]</span> Karl Menger. [Zur allgemeinen kurventheorie](http://yadda.icm.edu.pl/yadda/element/bwmeta1.element.bwnjournal-article-fmv10i1p2bwm). _Fundamenta Mathematicae_, 1(10):96--115, 1927.
  * <span id="MMP+19">\[MMP+19\]</span> Tomáš Masařík, Irene Muzi, Marcin Pilipczuk, Paweł Rzążewski, and Manuel Sorge. [Packing directed circuits quarter-integrally](https://arxiv.org/abs/1907.02494). _ArXiv preprint arXiv:1907.02494_, 2019.
  * <span id="MNL84">\[MNL85\]</span> Luis Montejano and Victor Neumann-Lara. [A variation of Menger's theorem for long paths](https://doi.org/10.1016/0095-8956(84)90026-1). _Journal of Combinatorial Theory, Series B_, 36(2):213--217, 1984.
  * <span id="MNŠW17">\[MNŠW17\]</span> Franck Mousset, Andreas Noever, Nemanja Škorić, and Felix Weissenberger. [A tight Erdős-Pósa function for long cycles](https://doi.org/10.1016/j.jctb.2017.01.004). _Journal of Combinatorial Theory, Series B_, 125:21--32, 2017.
  * <span id="MPT18">\[MPT18\]</span> Jessica McDonald, Gregory J. Puleo, Craig Tennenhouse. [Packing and covering directed triangles](https://doi.org/10.1007/s00373-020-02167-8). _Graphs and Combinatorics_, 3694):1059--1063, 2020.
  * <span id="Mun16">\[Mun16\]</span> Andrea Munaro. [On some classical and new hypergraph invariants](https://hal.archives-ouvertes.fr/tel-01680456v1). PhD Thesis. Université Grenoble Alpes, 2016.
  * <span id="MW15">\[MW15\]</span> Dániel Marx and Paul Wollan. [An exact characterization of tractable demand patterns for maximum disjoint path problems](https://doi.org/10.1137/1.9781611973730.44). In _Proceedings of the Twenty-Sixth Annual ACM-SIAM Symposium on Discrete Algorithms_, pages 642--661. SIAM, 2015.
  * <span id="MYZ13">\[MYZ13\]</span> Jie Ma, Xingxing Yu, and Wenan Zang. [Approximate min-max relations on plane graphs](https://doi.org/10.1007/s10878-011-9440-0). _Journal of Combinatorial Optimization_, 26(1):127--134, 2013.
  * <span id="Pul15">\[Pul15\]</span> Gregory J. Puleo. [Tuza’s Conjecture for Graphs with Maximum Average Degree less than 7](https://doi.org/10.1016/j.ejc.2015.03.006). _European Journal of Combinatorics_, 49:134--152, 2015.
  * <span id="PW12">\[PW12\]</span> Matteo Pontecorvi and Paul Wollan. [Disjoint cycles intersecting a set of vertices](https://doi.org/10.1016/j.jctb.2012.05.004). _Journal of Combinatorial Theory, Series B_, 102(5):1134 -- 1141, 2012.
  * <span id="Ray18">\[Ray18\]</span> Jean-Florent Raymond. [Hitting minors, subdivisions, and immersions in tournaments](https://dmtcs.episciences.org/4212). _Discrete Mathematics & Theoretical Computer Science_, 20(1):4212, 2018.
  * <span id="Ree99">\[Ree99\]</span> Bruce A. Reed. [Mangoes and blueberries](https://doi.org/10.1007/s004930050056). _Combinatorica_, 19(2):267--296, 1999.
  * <span id="RR01">\[RR01\]</span> Dieter Rautenbach and Bruce Reed. [The Erdős--Pósa property for odd cycles in highly connected graphs](http://doi.org/10.1007/s004930100024). _Combinatorica_, 21(2):267--278, 2001.
  * <span id="RRST96">\[RRST96\]</span> Bruce Reed, Neil Robertson, Paul Seymour, and Robin Thomas. [Packing directed circuits](http://doi.org/10.1007/BF01271272). _Combinatorica_, 16(4):535--554, 1996.
  * <span id="RS86">\[RS86\]</span> Neil Robertson and Paul D. Seymour. [Graph Minors. V. Excluding a planar graph](https://doi.org/10.1016/0095-8956(86)90030-4). _Journal of Combinatorial Theory, Series B_, 41(2):92--114, 1986.
  * <span id="RS96">\[RS96\]</span> Bruce A. Reed and Bruce F. Shepherd. [The Gallai-Younger conjecture for planar graphs](https://doi.org/10.1007/BF01271273). _Combinatorica_, 16(4):555--566, 1996.
  * <span id="RST16">\[RST16\]</span> Jean-Florent. Raymond, Ignasi. Sau, and Dimitrios M. Thilikos. [An edge variant of the Erdős-Pósa property](https://doi.org/10.1016/j.disc.2016.03.004). _Discrete Mathematics_, 339(8):2027 -- 2035, 2016.
  * <span id="RT13">\[RT13\]</span> Jean-Florent Raymond and Dimitrios M. Thilikos. [Polynomial gap extensions of the Erdős-Pósa theorem](https://arxiv.org/abs/1305.7376). In Jaroslav Nešetřil and Marco Pellegrini, editors, _The Seventh European Conference on Combinatorics, Graph Theory and Applications_, volume 16 of _CRM Series_, pages 13--18. Scuola Normale Superiore, 2013.
  * <span id="RT17">\[RT17\]</span> Jean-Florent Raymond and Dimitrios M. Thilikos. [Recent techniques and results on the Erdős-Pósa property](https://doi.org/10.1016/j.dam.2016.12.025). _Discrete Applied Mathematics_, 231:25-43, 2017.
  * <span id="Sam83">\[Sam83\]</span> E. Sampathkumar. [A generalization of Menger's theorem for trees](https://www.researchgate.net/profile/Jerzy_Topp2/publication/265589521_A_generalization_of_Menger%27s_theorem_for_trees/links/5668391208ae34c89a05dae5/A-generalization-of-Mengers-theorem-for-trees.pdf?origin=publication_detail). _Journal of Combinatorics, Information and System Sciences_, 8:78--80, 1983.
  * <span id="Sam92">\[Sam92\]</span> E. Sampathkumar. [A generalization of Menger's theorem for certain unicyclic graphs](https://doi.org/10.1007/BF02351593). _Graphs and Combinatorics_, 8(4):377--380, 1992.
  * <span id="Sch01">\[Sch01\]</span> Alexander Schrijver. [A short proof of Mader's $S$-paths theorem](https://doi.org/10.1006/jctb.2000.2029). _Journal of Combinatorial Theory, Series B_, 82(2):319--321, 2001.
  * <span id="Sey95">\[Sey95\]</span> Paul D. Seymour. [Packing directed circuits fractionally](https://doi.org/10.1007/BF01200760). _Combinatorica_, 15:281--288, 1995.
  * <span id="Sey96">\[Sey96\]</span> Paul D. Seymour. [Packing circuits in Eulerian digraphs](https://doi.org/10.1007/BF01844848). _Combinatorica_, 16(2):223--231, 1996.
  * <span id="Sim67">\[Sim67\]</span> Miklós Simonovits. [A new proof and generalizations of a theorem of Erdős and Pósa on graphs without $k+1$ independent circuits](https://doi.org/10.1007/BF02020974). _Acta Mathematica Academiae Scientiarum Hungarica_, 18(1):191--206, 1967.
  * <span id="SS04">\[SS04\]</span> András Sebő and László Szegő. [The path-packing structure of graphs](https://doi.org/10.1007/b97946). In Daniel Bienstock and George Nemhauser, editors, _Integer Programming and Combinatorial Optimization: 10th International IPCO Conference, New York, NY, USA, June 7-11, 2004. Proceedings_, pages 256--270, Berlin, Heidelberg, 2004. Springer Berlin Heidelberg.
  * <span id="STV22">\[STV22\]</span> Niklas Schlomber, Hanjo Thiele, and Jens Vygen. [Packing cycles in planar and bounded-genus graphs](https://arxiv.org/abs/2207.00450). _arXiv preprint_ arXiv:2207.00450, 2022.
  * <span id="SU20">\[SU20\]</span> Raphael Steck and Arthur Ulmer. [Long Ladders do not have the edge-Erdős-Pósa property](https://arxiv.org/abs/2003.03236). arXiv preprint arXiv:2003.03236, 2020.
  * <span id="Tho88">\[Tho88\]</span> Carsten Thomassen. [On the presence of disjoint subgraphs of a specified type](https://doi.org/10.1002/jgt.3190120111). _Journal of Graph Theory_, 12(1):101--111, 1988.
  * <span id="Tho01">\[Tho01\]</span> Carsten Thomassen. [The Erdős-Pósa property for odd cycles in graphs of large connectivity](https://doi.org/10.1007/s004930100028). _Combinatorica_, 21(2):321--333, 2001.
  * <span id="Tuz90">\[Tuz90\]</span> Zsolt Tuza. [A conjecture on triangles of graphs](https://doi.org/10.1007/BF01787705). _Graphs and Combinatorics_, 6(4):373--380, 1990.
  * <span id="Tuz94">\[Tuz94\]</span> Zsolt Tuza. [Perfect triangle families](https://doi.org/10.1112/blms/26.4.321). _Bulletin of the London Mathematical Society_, 26:321-324, 1994.
  * <span id="TV89">\[TV89\]</span> Jerzy Topp and Lutz Volkmann. [A generalization of Menger's theorem for trees](https://www.researchgate.net/profile/Jerzy_Topp2/publication/265589521_A_generalization_of_Menger%27s_theorem_for_trees/links/5668391208ae34c89a05dae5/A-generalization-of-Mengers-theorem-for-trees.pdf?origin=publication_detail). _Journal of Combinatorics, Information and System Sciences_, 14(4):249--250, 1989.
  * <span id="Ulm20">\[Ulm20\]</span> Arthur Ulmer. [Zero A-paths and the Erdős-Pósa property](https://arxiv.org/abs/2010.00663). arXiv e-print arXiv:2010.00663. October 2020.
  * <span id="Vos67">\[Vos67\]</span> Heinz-Jürgen Voss. Über die Anzahl von Knotenpunkten, die in einem Graphen, der keine drei unabhangige Kreise enthalt, alle Kreise reprasentieren. Wiss. Z. TH Ilmenau 13 Nr. 4/2, 413-418, 1967. Apparently impossible to find online but cited in [\[Vos69\]](#Vos69).
  * <span id="Vos68">\[Vos68\]</span> Heinz-Jürgen Voss. [Some properties of graphs containing $k$ independent circuits](https://scholar.google.fr/scholar?cluster=1691835029444520210). Theory of Graphs, Proceedings of the Colloquium held at Tihany, Hungary, September 1966. Akademiai
Kiadó. Publishing House of the Hungarian Academy of Sciences, 321-334, 1968.
  * <span id="Vos69">\[Vos69\]</span> Heinz-Jürgen Voss. [Eigenschaften von Graphen, die keine $k+1$ knotenfremde Kreise enthalten](https://doi.org/10.1002/mana.19690400104). Mathematische Nachrichten, 40:19-25, 1969.
  * <span id="Wei18">\[Wei18\]</span> Daniel Weißauer. [In absence of long chordless cycles, large tree-width becomes a local phenomenon](https://doi.org/10.1016/j.jctb.2019.04.004), _Journal of Combinatorial Theory, Series B_, 139:342-352, 2019.
  * <span id="Wol10">\[Wol10\]</span> Paul Wollan. [Packing non-zero $A$-paths in an undirected model of group labeled graphs](https://doi.org/10.1016/j.jctb.2009.05.003). _Journal of Combinatorial Theory, Series B_, 100(2): 141--150, 2010.
  * <span id="Wol11">\[Wol11\]</span> Paul Wollan. [Packing cycles with modularity constraints](https://doi.org/10.1007/s00493-011-2551-5). _Combinatorica_, 31(1):95--126, 2011.

Last updated: July 2022.
